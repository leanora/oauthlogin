<?php

namespace Leanora\Auth\Modules;

use Leanora\Contracts\Auth\AuthorizationModuleInterface;
use OAuth\Common\Consumer\Credentials;
use OAuth\Common\Http\Uri\UriFactory;
use OAuth\Common\Storage\Session;
use OAuth\OAuth2\Service\Google as Service;
use OAuth\ServiceFactory;

class Google implements AuthorizationModuleInterface
{

    /**
     * Instance of UriFactory
     * @var UriFactory
     */
    protected $currentUri;

    /**
     * Instance of ServiceFactory
     * @var ServiceFactory
     */
    protected $service;

    public function __construct($key, $secret, array $scope, $redirectUrl)
    {
        $this->setupService($key, $secret, $scope, $redirectUrl);
    }

    /**
     * Setup the google service factory
     *
     * @param  string   $key    Google api key
     * @param  string   $secret Google secret key
     * @param  array    $scope  Google api request scope
     * @return void
     */
    public function setupService($key, $secret, array $scope, $redirectUrl)
    {
        ini_set('date.timezone', 'Europe/Amsterdam');

        $uriFactory = new UriFactory();
        $this->currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
        $this->currentUri->setQuery('');

        $serviceFactory = new ServiceFactory();
        $storage = new Session();

        $credentials = new Credentials(
            $key,
            $secret,
            $redirectUrl
        );

        $this->service = $serviceFactory->createService('google', $credentials, $storage, $scope);
    }

    /**
     * Get data from api
     *
     * @param  string   $code   Code provided by Google api
     * @param  string   $state  State provided by Google api
     * @throws Exception        Throws new exception when code is empty
     * @return array|mixed
     */
    public function getData($code, $state)
    {
        if (empty($code)) {
            throw new Exception('Code is empty, cannot resume');
        }

        $state = isset($state) ? $state : null;

        $this->service->requestAccessToken($code, $state);

        return json_decode($this->service->request('userinfo'), true);
    }

    /**
     * Return the Google api authorization url
     *
     * @return string   Return an url to the google login page
     */
    public function getAuthorizationUrl()
    {
        return $this->service->getAuthorizationUri();
    }
}
