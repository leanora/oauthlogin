<?php

namespace Leanora\Auth;

use Leanora\Contracts\Auth\AuthorizationModuleInterface;

class Authorization
{
    /**
     * Instance of AuthorizationModuleInterface
     * @var AuthorizationModuleInterface
     */
    protected $module;

    public function __construct(AuthorizationModuleInterface $module)
    {
        $this->module = $module;
    }

    public function getData($code, $state)
    {
        return $this->module->getData($code, $state);
    }

    public function getAuthorizationUrl()
    {
        return $this->module->getAuthorizationUrl();
    }
}
