# Oauth login

Basic usage:

```php
<?php

use Leanora\Auth\Authorization;
use Leanora\Auth\Modules\Google;

require_once __DIR__ . '/vendor/autoload.php';

$key = '';
$secret = '';
$scope = ['userinfo_email', 'userinfo_profile'];
$redirectUrl = '';

$authorization = new Authorization(
    new Google(
        $key,
        $secret,
        $scope,
        $redirectUrl
    )
);

if (@$_GET['code']) {
    die('<pre>' . var_dump($authorization->getData($_GET['code'], $_GET['state'])) . '</pre>');
}

?>

<a href="<?php echo $authorization->getAuthorizationUrl() ?>">Google Login</a>
```